package net.bosted.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import net.bosted.domain.Record;
import net.bosted.domain.Records;

@Path("records")
public interface RecordsResource {

	@GET
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Records getList(@QueryParam("start") int start, @QueryParam("size") @DefaultValue("0") int size,
			@QueryParam("userId") @DefaultValue("0") int userId, @Context UriInfo uriInfo);

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Record get(@PathParam("id") int id);

	@POST
	@Consumes({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Response create(Record domain, @Context UriInfo uriInfo);

	@DELETE
	@Path("{id}")
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Record delete(@PathParam("id") int id);

	@PUT
	@Path("{id}")
	@Consumes({ "application/json", "application/xml", "application/x-java-serialized-object" })
	public void createOrUpdate(@PathParam("id") int id, Record domain);
}
