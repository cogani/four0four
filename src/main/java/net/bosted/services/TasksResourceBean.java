package net.bosted.services;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import net.bosted.domain.Link;
import net.bosted.domain.Person;
import net.bosted.domain.Task;
import net.bosted.domain.Tasks;
import net.bosted.persistence.PersonEntity;
import net.bosted.persistence.TaskEntity;

import org.springframework.transaction.annotation.Transactional;

public class TasksResourceBean implements TasksResource {
	private EntityManager em;

	// private @Autowired HttpServletRequest request;

	@PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "four0four")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	public Task get(int id) {
		TaskEntity task = em.getReference(TaskEntity.class, id);
		return entity2domain(task);
	}

	@Override
	@Transactional
	public Response create(Task task, UriInfo uriInfo, HttpServletRequest request) {

		// Transformer tf = TransformerFactory.newInstance().newTransformer();
		// tf.transform(new DOMSource(doc), new StreamResult(System.out));
		int size = 0;
		byte[] buffer = new byte[1024];
		try {
			while ((size = request.getInputStream().read(buffer)) != -1)
				System.out.write(buffer, 0, size);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TaskEntity entity = new TaskEntity();
		domain2entity(entity, task);
		em.persist(entity);
		em.refresh(entity);
		em.flush();
		// TODO Use entitytodomain
		task.setId(entity.getId());

		// System.out.println("Created task ->" + entity);
		return Response.created(URI.create("/four0four/services/tasks/" + entity.getId())).entity(task).build();
	}

	@Override
	@Transactional
	public Task delete(int id) {
		TaskEntity task = em.getReference(TaskEntity.class, id);

		if (task == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);

		em.remove(task);
		em.flush();

		return entity2domain(task);
	}

	public void domain2entity(TaskEntity entity, Task task) {
		entity.setId(task.getId());
		entity.setText(task.getText());
		entity.setStatus(task.getStatus());
		entity.setUserId(task.getUserId());
		Person person = task.getPerson();
		if (person != null) {
			PersonEntity personEntity = em.find(PersonEntity.class, person.getId());
			if (personEntity != null) {
				entity.setPersonEntity(personEntity);
			} else {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
		}
	}

	public static Task entity2domain(TaskEntity entity) {
		Task task = new Task();
		task.setId(entity.getId());
		task.setText(entity.getText());
		task.setStatus(entity.getStatus());
		task.setUserId(entity.getUserId());
		task.setPerson(PeopleResourceBean.entity2domain(entity.getPersonEntity()));
		return task;
	}

	@Override
	@Transactional
	public void createOrUpdate(int id, Task update) {
		TaskEntity task = em.getReference(TaskEntity.class, id);

		if (task == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);

		domain2entity(task, update);

		em.merge(task);
		em.flush();

	}

	@Override
	public Tasks getList(int start, int size, int userId, UriInfo uriInfo) {

		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.queryParam("start", "{start}");
		builder.queryParam("size", "{size}");
		// System.out.println("Builder.toString()->"+builder.toString());
		ArrayList<Task> list = new ArrayList<Task>();
		ArrayList<Link> links = new ArrayList<Link>();
		Query query;
		if (userId > 0) {
			query = em.createQuery("select c from task c where c.userId=:userId");
			query.setParameter("userId", userId);
		} else
			query = em.createQuery("select c from task c");

		List<TaskEntity> taskEntities = query.setFirstResult(start).setMaxResults(size).getResultList();

		for (Object obj : taskEntities) {
			TaskEntity entity = (TaskEntity) obj;
			list.add(entity2domain(entity));
		}
		// next link
		// If the size returned is equal then assume there is a next
		if (taskEntities.size() == size) {
			int next = start + size;
			URI nextUri = builder.clone().build(next, size);
			Link nextLink = new Link("next", nextUri.toString(), "application/xml");
			links.add(nextLink);
		}
		// previous link
		if (start > 0) {
			int previous = start - size;
			if (previous < 0)
				previous = 0;
			URI previousUri = builder.clone().build(previous, size);
			Link previousLink = new Link("previous", previousUri.toString(), "application/xml");
			links.add(previousLink);
		}

		Tasks tasks = new Tasks();
		tasks.setTasks(list);
		tasks.setLinks(links);
		return tasks;
	}

}