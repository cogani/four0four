package net.bosted.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import net.bosted.domain.Link;
import net.bosted.domain.People;
import net.bosted.domain.Person;
import net.bosted.persistence.PersonEntity;

import org.springframework.transaction.annotation.Transactional;

public class PeopleResourceBean implements PeopleResource {
	private EntityManager em;

	@PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "four0four")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	public Person get(int id) {
		PersonEntity person = em.getReference(PersonEntity.class, id);
		return entity2domain(person);
	}

	@Override
	@Transactional
	public Response create(Person person, UriInfo uriInfo) {
		PersonEntity entity = new PersonEntity();
		domain2entity(entity, person);
		em.persist(entity);
		em.refresh(entity);
		em.flush();
		// TODO Use entitytodomain
		person.setId(entity.getPersonId());

		System.out.println("Created person ->" + entity);
		return Response.created(URI.create("/four0four/services/people/" + entity.getPersonId())).entity(person).build();
	}

	@Override
	@Transactional
	public Person delete(int id) {
		PersonEntity person = em.getReference(PersonEntity.class, id);

		if (person == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);

		em.remove(person);
		em.flush();

		return entity2domain(person);
	}

	public static void domain2entity(PersonEntity entity, Person person) {
		entity.setPersonId(person.getId());
		entity.setFirstName(person.getFirstName());
		entity.setLastName(person.getLastName());
	}

	public static Person entity2domain(PersonEntity entity) {
		Person person = new Person();
		person.setId(entity.getPersonId());
		person.setFirstName(entity.getFirstName());
		person.setLastName(entity.getLastName());

		return person;
	}

	@Override
	@Transactional
	public void createOrUpdate(int id, Person update) {
		PersonEntity person = em.getReference(PersonEntity.class, id);

		if (person == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);

		person.setFirstName(update.getFirstName());
		person.setLastName(update.getLastName());
		em.merge(person);
		em.flush();

	}

	@Override
	public People getList(int start, int size, UriInfo uriInfo) {

		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.queryParam("start", "{start}");
		builder.queryParam("size", "{size}");
		// System.out.println("Builder.toString()->"+builder.toString());
		ArrayList<Person> list = new ArrayList<Person>();
		ArrayList<Link> links = new ArrayList<Link>();

		Query query = null;
		// if (firstName != null && lastName != null) {
		// query =
		// em.createQuery("select c from person c where c.firstName=:first and c.lastName=:last");
		// query.setParameter("first", firstName);
		// query.setParameter("last", lastName);
		//
		// } else if (lastName != null) {
		// query =
		// em.createQuery("select c from person c where c.lastName=:last");
		// query.setParameter("last", lastName);
		// } else {
		// query = em.createQuery("select c from person c");
		// }

		query = em.createQuery("select c from person c");

		List<PersonEntity> personEntities = query.setFirstResult(start).setMaxResults(size).getResultList();

		for (Object obj : personEntities) {
			PersonEntity entity = (PersonEntity) obj;
			list.add(entity2domain(entity));
		}
		// next link
		// If the size returned is equal then assume there is a next
		if (personEntities.size() == size) {
			int next = start + size;
			URI nextUri = builder.clone().build(next, size);
			Link nextLink = new Link("next", nextUri.toString(), "application/xml");
			links.add(nextLink);
		}
		// previous link
		if (start > 0) {
			int previous = start - size;
			if (previous < 0)
				previous = 0;
			URI previousUri = builder.clone().build(previous, size);
			Link previousLink = new Link("previous", previousUri.toString(), "application/xml");
			links.add(previousLink);
		}
		People customers = new People();
		customers.setCustomers(list);
		customers.setLinks(links);
		return customers;
	}

}
