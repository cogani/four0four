package net.bosted.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import net.bosted.domain.Link;
import net.bosted.domain.Person;
import net.bosted.domain.Record;
import net.bosted.domain.Records;
import net.bosted.persistence.PersonEntity;
import net.bosted.persistence.RecordEntity;

import org.springframework.transaction.annotation.Transactional;

public class RecordsResourceBean implements RecordsResource {
	private EntityManager em;

	@PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "four0four")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	public Record get(int id) {
		RecordEntity record = em.getReference(RecordEntity.class, id);
		return entity2domain(record);
	}

	@Override
	@Transactional
	public Response create(Record record, UriInfo uriInfo) {
		RecordEntity entity = new RecordEntity();
		domain2entity(entity, record);
		em.persist(entity);
		em.refresh(entity);
		em.flush();
		// TODO Use entitytodomain
		record.setRecordId(entity.getRecordId());

		// System.out.println("Created record ->" + entity);
		return Response.created(URI.create("/four0four/services/records/" + entity.getRecordId())).entity(record).build();
	}

	@Override
	@Transactional
	public Record delete(int id) {
		RecordEntity record = em.getReference(RecordEntity.class, id);

		if (record == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);

		em.remove(record);
		em.flush();

		return entity2domain(record);
	}

	public void domain2entity(RecordEntity entity, Record record) {
		entity.setRecordId(record.getRecordId());
		entity.setText(record.getText());
		entity.setSourceId(record.getSourceId());
		entity.setUserId(record.getUserId());
		entity.setSourceType(record.getSourceType());
		// PersonEntity personEntity = new PersonEntity();
		// PeopleResourceBean.domain2entity(personEntity, record.getPerson());
		// entity.setPersonEntity(personEntity);

		Person person = record.getPerson();
		if (person != null) {
			PersonEntity personEntity = em.find(PersonEntity.class, person.getId());
			if (personEntity != null) {
				entity.setPersonEntity(personEntity);
			} else {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
		}

	}

	public static Record entity2domain(RecordEntity entity) {
		Record record = new Record();
		record.setRecordId(entity.getRecordId());
		record.setText(entity.getText());
		record.setSourceId(entity.getSourceId());
		record.setUserId(entity.getUserId());
		record.setSourceType(entity.getSourceType());
		record.setPerson(PeopleResourceBean.entity2domain(entity.getPersonEntity()));
		return record;
	}

	@Override
	@Transactional
	public void createOrUpdate(int id, Record update) {
		RecordEntity record = em.getReference(RecordEntity.class, id);

		if (record == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);

		domain2entity(record, update);

		em.merge(record);
		em.flush();

	}

	@Override
	public Records getList(int start, int size, int userId, UriInfo uriInfo) {

		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.queryParam("start", "{start}");
		builder.queryParam("size", "{size}");
		// System.out.println("Builder.toString()->"+builder.toString());
		ArrayList<Record> list = new ArrayList<>();
		ArrayList<Link> links = new ArrayList<>();
		Query query;
		if (userId > 0) {
			query = em.createQuery("select c from record c where c.userId=:userId");
			query.setParameter("userId", userId);
		} else
			query = em.createQuery("select c from record c");

		List<RecordEntity> recordEntities = query.setFirstResult(start).setMaxResults(size).getResultList();

		for (Object obj : recordEntities) {
			RecordEntity entity = (RecordEntity) obj;
			list.add(entity2domain(entity));
		}
		// next link
		// If the size returned is equal then assume there is a next
		if (recordEntities.size() == size) {
			int next = start + size;
			URI nextUri = builder.clone().build(next, size);
			Link nextLink = new Link("next", nextUri.toString(), "application/xml");
			links.add(nextLink);
		}
		// previous link
		if (start > 0) {
			int previous = start - size;
			if (previous < 0)
				previous = 0;
			URI previousUri = builder.clone().build(previous, size);
			Link previousLink = new Link("previous", previousUri.toString(), "application/xml");
			links.add(previousLink);
		}

		Records records = new Records();
		records.setRecords(list);
		records.setLinks(links);
		return records;
	}

}