package net.bosted.services;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import net.bosted.domain.Task;
import net.bosted.domain.Tasks;

@Path("tasks")
public interface TasksResource {

	@GET
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Tasks getList(@QueryParam("start") int start, @QueryParam("size") @DefaultValue("0") int size,
			@QueryParam("userId") @DefaultValue("0") int userId, @Context UriInfo uriInfo);

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Task get(@PathParam("id") int id);

	@POST
	@Consumes({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Response create(Task domain, @Context UriInfo uriInfo, @Context HttpServletRequest request);

	@DELETE
	@Path("{id}")
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Task delete(@PathParam("id") int id);

	@PUT
	@Path("{id}")
	@Consumes({ "application/json", "application/xml", "application/x-java-serialized-object" })
	public void createOrUpdate(@PathParam("id") int id, Task domain);
}
