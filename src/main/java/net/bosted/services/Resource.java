package net.bosted.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

public interface Resource<DomainType, DomainCollectionType> {
	@GET
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	DomainCollectionType getList(@QueryParam("start") int start, @QueryParam("size") @DefaultValue("0") int size, @Context UriInfo uriInfo);

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	DomainType get(@PathParam("id") int id);

	@POST
	@Consumes({ "application/json", "application/xml", "application/x-java-serialized-object" })
	Response create(DomainType domain, @Context UriInfo uriInfo);

	@DELETE
	@Path("{id}")
	@Produces({ "application/json", "application/xml", "application/x-java-serialized-object" })
	DomainType delete(@PathParam("id") int id);

	@PUT
	@Path("{id}")
	@Consumes({ "application/json", "application/xml", "application/x-java-serialized-object" })
	public void createOrUpdate(@PathParam("id") int id, DomainType domain);
}
