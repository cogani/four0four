package net.bosted.persistence;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import net.bosted.domain.SourceType;

@Entity(name = "record")
public class RecordEntity {
	private int recordId;
	private int sourceId;
	private SourceType sourceType;
	private String text;
	// private media;
	private PersonEntity personEntity;
	private int userId;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	@Enumerated(EnumType.STRING)
	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PersonEntity.class)
	@JoinColumn(name = "personId")
	public PersonEntity getPersonEntity() {
		return personEntity;
	}

	public void setPersonEntity(PersonEntity person) {
		this.personEntity = person;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public int getSourceId() {
		return sourceId;
	}

	public RecordEntity() {
		super();
	}

}