package net.bosted.persistence;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "person")
public class PersonEntity {

	private int personId;
	private String firstName;
	private String lastName;

	private List<TaskEntity> tasks;
	private List<RecordEntity> records;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int id) {
		this.personId = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personEntity", targetEntity = TaskEntity.class)
	public List<TaskEntity> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskEntity> tasks) {
		this.tasks = tasks;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personEntity", targetEntity = RecordEntity.class)
	public List<RecordEntity> getRecords() {
		return records;
	}

	public void setRecords(List<RecordEntity> records) {
		this.records = records;
	}
	
	
	public PersonEntity() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + personId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonEntity other = (PersonEntity) obj;
		if (personId != other.personId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PersonEntity [id=" + personId + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
}