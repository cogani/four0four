package net.bosted.apps;

import java.util.HashSet;
import java.util.Set;

import javax.naming.InitialContext;
import javax.ws.rs.core.Application;

import net.bosted.services.EntityNotFoundExceptionMapper;
import net.bosted.services.JavaMarshaller;
import net.bosted.services.PeopleResourceBean;
import net.bosted.services.RecordsResourceBean;
import net.bosted.services.TasksResourceBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//@ApplicationPath("/*")
public class FrontApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();
	protected ApplicationContext springContext;

	public FrontApplication() {
		singletons.add(new PeopleResourceBean());
		singletons.add(new TasksResourceBean());
		singletons.add(new RecordsResourceBean());
		classes.add(EntityNotFoundExceptionMapper.class);
		classes.add(JavaMarshaller.class);
	}

	public Set<Class<?>> getClasses() {
		return classes;
	}

	public Set<Object> getSingletons() {
		System.err.println("getSingletons");
		try {
			InitialContext ctx = new InitialContext();
			String xmlFile = (String) ctx.lookup("java:comp/env/spring-beans-file");
			springContext = new ClassPathXmlApplicationContext(xmlFile);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		HashSet<Object> set = new HashSet<>();
		set.add(springContext.getBean("person"));
		set.add(springContext.getBean("task"));
		set.add(springContext.getBean("record"));

		return set;
	}
}
