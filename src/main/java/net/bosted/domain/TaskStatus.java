package net.bosted.domain;

public enum TaskStatus {
	PENDING, COMPLETED, DISCARDED
}
