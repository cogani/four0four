package net.bosted.domain;

public enum SourceType {
	TASK, ROUTINE, PERSON
}
