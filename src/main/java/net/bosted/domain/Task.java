package net.bosted.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "task")
@XmlType
public class Task implements Serializable {
	private static final long serialVersionUID = -3518449251433717481L;
	private int id;
	private String text;
	private TaskStatus status;
	private Person person;
	private int userId;

	@XmlAttribute
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@XmlElement(name = "text")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@XmlElement(name = "status")
	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}
	
	@XmlElement
	public int getPersonId() {
		return person.getId();
	}

	public void setPersonId(int personId) {
		person = new Person();
		person.setId(personId);
	}

	// @XmlElement(name = "person")
	// @XmlIDREF
	@XmlTransient
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@XmlAttribute
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Task() {
		super();
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", text=" + text + ", status=" + status + ", person=" + person + ", userId=" + userId + "]";
	}

}