package net.bosted.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "person")
@XmlType
public class Person implements Serializable {
	private static final long serialVersionUID = 1676696606625781189L;
	@XmlTransient
	private int id;
	private String firstName;
	private String lastName;

	// @XmlAttribute
	// @XmlID
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@XmlAttribute(name = "id")
//	@XmlID
	public String getStrId() {
		return Integer.toString(id);
	}

	public void setId(String id) {
		this.id = Integer.parseInt(id);
	}

	// @XmlAttribute(name = "first-name")
	@XmlAttribute
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	// @XmlAttribute(name = "last-name")
	@XmlAttribute
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

	public Person() {
		super();
	}

}