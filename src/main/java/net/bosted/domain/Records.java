package net.bosted.domain;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "records")
public class Records {
	protected Collection<Record> records = new ArrayList<Record>();
	protected List<Link> links;

	@XmlElementRef
	public Collection<Record> getRecords() {
		return records;
	}

	public void setRecords(Collection<Record> records) {
		this.records = records;
	}

	@XmlAttribute
	public int getCount() {
		return records.size();
	}

	@XmlElementRef
	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@XmlTransient
	public URI getNext() throws URISyntaxException {
		if (links == null)
			return null;
		for (Link link : links) {
			if ("next".equals(link.getRelationship()))
				return new URI(link.getHref());
		}
		return null;
	}

	@XmlTransient
	public URI getPrevious() throws URISyntaxException {
		if (links == null)
			return null;
		for (Link link : links) {
			if ("previous".equals(link.getRelationship()))
				return new URI(link.getHref());
		}
		return null;
	}

}
