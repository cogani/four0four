package net.bosted.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.eclipse.persistence.oxm.annotations.XmlCDATA;

@XmlRootElement(name = "record")
@XmlType
public class Record implements Serializable {

	private static final long serialVersionUID = 4077809426868720187L;
	private int recordId;
	private int sourceId;
	private SourceType sourceType;
	private String text;
	// private media;
	private Person person;
	private int userId;

	public Record() {
		super();
	}

	@XmlAttribute
	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	@XmlElement
	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	@XmlElement
	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	@XmlCDATA
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@XmlElement
	public int getPersonId() {
		return person.getId();
	}

	public void setPersonId(int personId) {
		person = new Person();
		person.setId(personId);
	}

	// @XmlElement(name = "person")
	// @XmlIDREF
	@XmlTransient
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@XmlElement
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}