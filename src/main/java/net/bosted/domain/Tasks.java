package net.bosted.domain;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "tasks")
public class Tasks {
	protected Collection<Task> tasks = new ArrayList<Task>();
	protected List<Link> links;

	@XmlElementRef
	public Collection<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Collection<Task> tasks) {
		this.tasks = tasks;
	}

	@XmlAttribute
	public int getCount() {
		return tasks.size();
	}

	@XmlElementRef
	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@XmlTransient
	public URI getNext() throws URISyntaxException {
		if (links == null)
			return null;
		for (Link link : links) {
			if ("next".equals(link.getRelationship()))
				return new URI(link.getHref());
		}
		return null;
	}

	@XmlTransient
	public URI getPrevious() throws URISyntaxException {
		if (links == null)
			return null;
		for (Link link : links) {
			if ("previous".equals(link.getRelationship()))
				return new URI(link.getHref());
		}
		return null;
	}

}
