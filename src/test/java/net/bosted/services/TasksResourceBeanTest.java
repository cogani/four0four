package net.bosted.services;

import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import net.bosted.domain.Person;
import net.bosted.domain.Task;
import net.bosted.domain.TaskStatus;
import net.bosted.domain.Tasks;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TasksResourceBeanTest {
	private static String taskLink = "http://localhost:8080/four0four/services/tasks";
	private static Client client;
	private String taskLocation;
	private static Task patternTask;

	@BeforeClass
	public static void initClient() throws IOException {
		client = ClientBuilder.newClient();
		client.register(JavaMarshaller.class);
	}

	@AfterClass
	public static void closeClient() {
		client.close();
	}

	@Before
	public void setupTest() throws IOException, JAXBException {
		patternTask = createNewTask(MediaType.APPLICATION_XML);
		taskLocation = taskLink + "/" + patternTask.getId();

	}

	@After
	public void tearUpTest() throws IOException {
		deleteTask().disconnect();
	}

	@Test
	public void testGetTaskAsXML() throws IOException, ClassNotFoundException {
		String mediaTypeTarget = MediaType.APPLICATION_XML;

		Task unmarshalledTask = testGetTaskAsMediaType(mediaTypeTarget);
	}

	@Test
	public void testGetTaskAsJSON() throws IOException, ClassNotFoundException {
		String mediaTypeTarget = MediaType.APPLICATION_JSON;

		Task unmarshalledTask = testGetTaskAsMediaType(mediaTypeTarget);

		System.out.println(unmarshalledTask);
	}

	private Task testGetTaskAsMediaType(String mediaTypeTarget) {
		WebTarget target = client.target(taskLocation);
		Response response = target.request(mediaTypeTarget).get();
		Assert.assertEquals(HttpURLConnection.HTTP_OK, response.getStatus());
		Assert.assertEquals(mediaTypeTarget, response.getMediaType().toString());
		// System.out.println(response.readEntity(String.class));
		Task unmarshalledTask = response.readEntity(Task.class);
		Assert.assertNotNull(patternTask);
		Assert.assertEquals(patternTask.getText(), unmarshalledTask.getText());
		Assert.assertEquals(patternTask.getStatus(), unmarshalledTask.getStatus());
		return unmarshalledTask;
	}

	@Test
	public void testCreateNewTaskFromXML() throws Exception {
		URL postUrl = new URL(taskLink);
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();
		// = createNewPerson(MediaType.APPLICATION_XML);

		Person person = new Person();
		person.setId(1);
		person.setFirstName("Peter");
		person.setLastName("Carter");

		Task task = new Task();

		task.setPerson(person);
		task.setStatus(TaskStatus.COMPLETED);
		task.setText("Deeemo text");
		task.setUserId(2);

		Response response = client.target(taskLink).request().post(Entity.entity(task, MediaType.APPLICATION_XML));

		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, response.getStatus());

		System.out.println(response.getEntity());
		connection.disconnect();
	}

	@Test
	public void testCreateNewTaskFromObject() throws Exception {
		String mediaTypeTarget = "application/x-java-serialized-object";

		Task unmarshalledTask = testGetTaskAsMediaType(mediaTypeTarget);

		System.out.println(unmarshalledTask);
	}

	private Task createNewTask(String mediaType) throws MalformedURLException, IOException, ProtocolException {

		Person person = new Person();
		person.setId(1);
		person.setFirstName("Peter");
		person.setLastName("Carter");

		Task task = new Task();

		task.setPerson(person);
		task.setStatus(TaskStatus.COMPLETED);
		task.setText("Deeemo text");
		task.setUserId(2);

		Response response = client.target(taskLink).request().post(Entity.entity(task, mediaType));
		return response.readEntity(Task.class);
	}

	@Test
	public void testDeleteTask() throws IOException, ClassNotFoundException {
		HttpURLConnection connection = deleteTask();

		Assert.assertEquals(MediaType.APPLICATION_XML, connection.getContentType());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, connection.getResponseCode());

		connection.disconnect();

	}

	private HttpURLConnection deleteTask() throws MalformedURLException, IOException, ProtocolException {
		URL getUrl = new URL(taskLocation);

		HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
		connection.setRequestMethod("DELETE");
		connection.setRequestProperty("Accept", MediaType.APPLICATION_XML);
		return connection;
	}

	@Test
	public void testUpdateTask() throws Exception {
		URL getURL = new URL(taskLocation);
		HttpURLConnection connection = (HttpURLConnection) getURL.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/x-java-serialized-object");
		ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());

		Task taskToUpdate = (Task) ois.readObject();
		connection.disconnect();

		taskToUpdate.setText("UPDATE");

		Response response = client.target(taskLocation).request().put(Entity.entity(taskToUpdate, "application/x-java-serialized-object"));
		if (response.getStatus() != 204)
			throw new RuntimeException("Failed to update");
	}

	@Test
	public void testQueryTasks() throws Exception {
		URI uri = new URI(taskLink);
		while (uri != null) {
			WebTarget target = client.target(uri);
			Tasks tasks = target.request().get(Tasks.class);
			uri = tasks.getNext();
		}

		assertNull(uri);
	}
}