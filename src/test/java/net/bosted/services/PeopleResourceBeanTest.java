package net.bosted.services;

import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import net.bosted.domain.People;
import net.bosted.domain.Person;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PeopleResourceBeanTest {
	static String peopleLink = "http://localhost:8080/four0four/services/people";
	private static Client client;
	private String personLocation;
	private static Person patternPerson;

	@BeforeClass
	public static void initClient() throws IOException {
		client = ClientBuilder.newClient();
		client.register(JavaMarshaller.class);
	}

	@AfterClass
	public static void closeClient() {
		client.close();
	}

	@Before
	public void setupTest() throws IOException, JAXBException {
		patternPerson = createNewPerson(MediaType.APPLICATION_XML);
		personLocation = peopleLink + "/" + patternPerson.getId();

	}

	@After
	public void tearUpTest() throws IOException {
		deletePerson().disconnect();
	}

	@Test
	public void testGetPersonAsXML() throws IOException, ClassNotFoundException {
		String mediaTypeTarget = MediaType.APPLICATION_XML;

		Person unmarshalledPerson = testGetPersonAsMediaType(mediaTypeTarget);
	}

	@Test
	public void testGetPersonAsJSON() throws IOException, ClassNotFoundException {
		String mediaTypeTarget = MediaType.APPLICATION_JSON;

		Person unmarshalledPerson = testGetPersonAsMediaType(mediaTypeTarget);

		System.out.println(unmarshalledPerson);
	}

	private Person testGetPersonAsMediaType(String mediaTypeTarget) {
		WebTarget target = client.target(personLocation);
		Response response = target.request(mediaTypeTarget).get();
		Assert.assertEquals(HttpURLConnection.HTTP_OK, response.getStatus());
		Assert.assertEquals(mediaTypeTarget, response.getMediaType().toString());
		// System.out.println(response.readEntity(String.class));
		Person unmarshalledPerson = response.readEntity(Person.class);
		Assert.assertNotNull(patternPerson);
		Assert.assertEquals(patternPerson.getFirstName(), unmarshalledPerson.getFirstName());
		Assert.assertEquals(patternPerson.getLastName(), unmarshalledPerson.getLastName());
		return unmarshalledPerson;
	}

	@Test
	public void testCreateNewPersonFromXML() throws Exception {
		URL postUrl = new URL(peopleLink);
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();
		// = createNewPerson(MediaType.APPLICATION_XML);

		Person person = new Person();
		person.setFirstName("Amandeep");
		person.setLastName("Varun");

		Response response = client.target(peopleLink).request().post(Entity.entity(person, MediaType.APPLICATION_XML));

		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, response.getStatus());

		System.out.println(response.getEntity());
		connection.disconnect();
	}

	@Test
	public void testCreateNewPersonFromObject() throws Exception {
		String mediaTypeTarget = "application/x-java-serialized-object";

		Person unmarshalledPerson = testGetPersonAsMediaType(mediaTypeTarget);

		System.out.println(unmarshalledPerson);
	}

	private Person createNewPerson(String mediaType) throws MalformedURLException, IOException, ProtocolException {

		Person person = new Person();
		person.setFirstName("Amandeep");
		person.setLastName("Varun");

		Response response = client.target(peopleLink).request().post(Entity.entity(person, mediaType));
		return response.readEntity(Person.class);
	}

	@Test
	public void testDeletePerson() throws IOException, ClassNotFoundException {
		HttpURLConnection connection = deletePerson();

		Assert.assertEquals(MediaType.APPLICATION_XML, connection.getContentType());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, connection.getResponseCode());

		connection.disconnect();

	}

	private HttpURLConnection deletePerson() throws MalformedURLException, IOException, ProtocolException {
		URL getUrl = new URL(personLocation);

		HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
		connection.setRequestMethod("DELETE");
		connection.setRequestProperty("Accept", MediaType.APPLICATION_XML);
		return connection;
	}

	@Test
	public void testUpdatePerson() throws Exception {
		URL getURL = new URL(personLocation);
		HttpURLConnection connection = (HttpURLConnection) getURL.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/x-java-serialized-object");
		ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());

		Person customerToUpdate = (Person) ois.readObject();
		connection.disconnect();

		customerToUpdate.setLastName("UPDATE");

		Response response = client.target(personLocation).request().put(Entity.entity(customerToUpdate, "application/x-java-serialized-object"));
		if (response.getStatus() != 204)
			throw new RuntimeException("Failed to update");
	}

	@Test
	public void testQueryPeople() throws Exception {
		URI uri = new URI(peopleLink);
		while (uri != null) {
			WebTarget target = client.target(uri);
			People person = target.request().get(People.class);
			uri = person.getNext();
		}

		assertNull(uri);
	}
}